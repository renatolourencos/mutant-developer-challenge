const request = require('request');

function getAllUsers(callback) {
    request(process.env.REPO_URL, (err, res, body) => {
        callback(err, body);
    });
}

module.exports = {
    getAllUsers
};