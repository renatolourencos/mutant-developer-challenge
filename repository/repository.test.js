const test = require('tape')
const repository = require('./repository')

function runTests() {

    test('Repository GetAllUsers', (t) => {
        repository.getAllUsers((err, users) => {
            t.assert(!err && users && users.length > 0, 'All Users Retorned')
            t.end()
        })
    })
}

module.exports = { runTests }