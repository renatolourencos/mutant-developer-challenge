require('dotenv-safe').config()

const users = require('./api/users')
const server = require('./server/server')
const repository = require('./repository/repository')

server.start(users, repository, (err, app) => {
    console.log('Just started')
})