require('dotenv-safe').config()
require('./repository/repository.test').runTests()
require('./server/server.test').runTests()
require('./api/users.test').runTests()