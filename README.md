Essa aplicação é uma teste de desenvolvimento para um processo seletivo.

Essa é uma API-Rest desenvolvida em NodeJS e deve-se seguir os seguintes passos para executa-lá:

1 - Clonar a aplicação do git para sua maquina.
2 - Acessar a pasta da aplicação que foi criada pelo git.
3 - Abirar a pasta do projeto em um editor de código.
4 - Copiar o arquivo .env.example e colar na raiz do projeto, deve-se remover o '.example' do arquivo.
5 - Configurar o arquivo com a porta que o servidor nodeJS irá expor quando estiver executando (normalmente porta 8080).
6 - Configurar URL do repositório que irá fornecer os dados a serem processados pela aplicação (https://jsonplaceholder.typicode.com/users).

Se já tem docke instalado em sua maquina, basta executar o comando para criar um container e a aplicação estará em execução.
Se não tem docker instalado, deverá instalar o nodeJS em sua maquina caso não o tenha e executar o comando npm install e assim que o mesmo terminar sua execução, executar o comando npm start.

Obs.: Caso queira executar os testes unitários, basta executar o comando npm test antes de executar o comando npm start.

As rotas exposta por essas aplicação são as seguintes:

 - localhost:"porta_que_foi_configurada_no_item_5"/users
        Retorna todo os usuários cadastrados no repositório.
 - localhost:"porta_que_foi_configurada_no_item_5"/users/info
        Retorna somente os campos "Nome, E-Mail e Nome da Empresa em que trabalha" de todos os usuários cadastrados no repositório.
 - localhost:"porta_que_foi_configurada_no_item_5"/users/suite
        Retorna somente os usuários que possuem a palavra "Suite" em seu endereço.
 - localhost:"porta_que_foi_configurada_no_item_5"/users/website
        Retorna o endereço do Website de todos os usuários cadastrados no repositório.