module.exports = (app, repository) => {
    app.get('/users', (req, res, next) => {
        repository.getAllUsers((err, users) => {
            if (err) return next(err);
            res.json(JSON.parse(users));
        });
    });

    app.get('/users/info', (req, res, next) => {
        repository.getAllUsers((err, users) => {
            if (err) return next(err);
            const userList = JSON.parse(users);
            let newUserList = [];

            userList.forEach((user, idx, userList) => {
                const filteredUserInfo = {};

                if (user.hasOwnProperty('name') && user.hasOwnProperty('email') && user.hasOwnProperty('company')) {
                    if (user['company'].hasOwnProperty('name')) {
                        filteredUserInfo['name'] = user['name'];
                        filteredUserInfo['email'] = user['email'];
                        filteredUserInfo['company_name'] = user['company']['name'];
                        newUserList.push(filteredUserInfo);
                    }
                }
                if (idx === userList.length - 1) {
                    newUserList = newUserList.sort((a, b) => {
                        const nameA = a.name.toLowerCase();
                        const nameB = b.name.toLowerCase();
                        if (nameA < nameB) return -1;
                        if (nameA > nameB) return 1;
                        return 0;
                    });
                }
            });
            res.json(newUserList);
        });
    });

    app.get('/users/suite', (req, res, next) => {
        repository.getAllUsers((err, users) => {
            if (err) return next(err);
            const userList = JSON.parse(users);
            let newUserList = [];

            userList.forEach((user) => {
                if (user.hasOwnProperty('address')) {
                    if (user['address'].hasOwnProperty('suite')) {
                        if (user['address']['suite'].match(/suite/i)) {
                            newUserList.push(user);
                        }
                    }
                }
            });
            res.json(newUserList);
        });
    });

    app.get('/users/website', (req, res, next) => {
        repository.getAllUsers((err, users) => {
            if (err) return next(err);
            const userList = JSON.parse(users);
            const websiteList = [];

            userList.forEach(user => {
                if (user.hasOwnProperty('website')) {
                    websiteList.push(user['website'])
                }
            });
            res.json(websiteList);
        });
    });

};