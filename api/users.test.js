const test = require('tape');
const supertest = require('supertest');
const users = require('./users');
const server = require("../server/server");
const repository = require("../repository/repository");

function runTests() {

    let app = null;

    server.start(users, repository, (err, app) => {
        let id = null;
        test('GET /users', (t) => {
            supertest(app)
                .get('/users')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    if (res.body && res.body.length > 0) id = res.body[0]._id;
                    t.error(err, 'No errors')
                    t.assert(res.body && res.body.length > 0, "All Users returned")
                    t.end()
                });
        });

        test('GET /users/info', (t) => {
            supertest(app)
                .get('/users/info')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    t.error(err, 'No errors')
                    t.assert(res.body, "Users Info returned")
                    t.end()
                });
        });

        test('GET /users/suite', (t) => {
            supertest(app)
                .get('/users/suite')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    t.error(err, 'No errors')
                    t.assert(res.body && res.body.length > 0, "Users With 'Suite' in Address returned")
                    t.end()
                });
        });

        test('GET /users/website', (t) => {
            supertest(app)
                .get('/users/website')
                .expect('Content-Type', /json/)
                .expect(200)
                .end((err, res) => {
                    t.error(err, 'No errors')
                    t.assert(res.body && res.body.length > 0, "Users Website Address returned")
                    t.end()
                });
        });

        server.stop()
    })
}

module.exports = {
    runTests
}